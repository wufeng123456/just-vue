import fetch from './fetch.js'

export function login () {
  return fetch({
    url: '/user/login',
    method: 'post'
  })
}
