export default function (file) {
  return () => import('@/components/' + file + '.vue')
}
