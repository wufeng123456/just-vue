/* eslint-disable */
import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/home';
const _import = require('./import').default
// const _import = function (file) {
//   return require('@/components/' + file + '.vue').default
// }
Vue.use(Router)
const routerMap = [
  {
    path: '/logicalValidator',
    name: 'logicalValidator',
    component: () => import('@views/logical-validator/index.vue')
  }
]
export default new Router({
  routes: routerMap
})
